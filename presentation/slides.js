import React from 'react';
import { Slide, Text, Heading, Table, TableItem } from 'spectacle';
import { MDXProvider } from '@mdx-js/tag'
import components from './components';
import theme from './theme';

import splashImage from '../assets/autumn-bg.jpg';

// SPLASH SCREEN
const h1 = ({ children }) => <Heading size={1}  textColor={theme.screen.colors.primary}>{children}</Heading>;

export const SplashSlide = ({ children, ...rest }) => (
  <Slide bgImage={splashImage} {...rest}>
    <MDXProvider components={{ ...components, h1 }}>{children}</MDXProvider>
  </Slide>
);

// DEFAULT LAYOUT

export const DefaultSlide = ({ children, ...rest }) => (
  <Slide {...rest}>
    <MDXProvider components={components}>{children}</MDXProvider>
  </Slide>
);

// DARK LAYOUT

const darkComponents = {
  ...components,
  h2: ({ children }) => <Heading size={2} textColor={theme.screen.colors.quaternary}>{children}</Heading>,
  h3: ({ children }) => <Heading size={3} textColor={theme.screen.colors.quaternary}>{children}</Heading>,
  h4: ({ children }) => <Heading size={4} textColor={theme.screen.colors.quaternary}>{children}</Heading>,
  h5: ({ children }) => <Heading size={5} textColor={theme.screen.colors.quaternary}>{children}</Heading>,
  h6: ({ children }) => <Heading size={6} textColor={theme.screen.colors.quaternary}>{children}</Heading>,
  p: ({ children }) => <Text textColor="white">{children}</Text>
}

export const DarkSlide = ({ children, ...rest }) => (
  <Slide bgColor="black" {...rest}>
    <MDXProvider components={darkComponents}>{children}</MDXProvider>
  </Slide>
);

// CODE LAYOUT

export const CodeSlide = ({ children, ...rest }) => (
  <Slide bgColor="#1d1f21" {...rest}>
    <MDXProvider components={components}>{children}</MDXProvider>
  </Slide>
);

// BIG TABLE LAYOUT

const table = ({ children }) => <Table textSize="18px">{children}</Table>;
const th = ({ children }) => <TableItem textSize="18px" bold>{children}</TableItem>;
const td = ({ children }) => <TableItem textSize="18px" textAlign="right">{children}</TableItem>;

export const BigTableSlide = ({ children, ...rest }) => (
  <Slide {...rest}>
    <MDXProvider components={{ ...components, th, td }}>{children}</MDXProvider>
  </Slide>
);

